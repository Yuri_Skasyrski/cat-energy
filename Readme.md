# Личный проект «Кэт энерджи» [![Build status][travis-image]][travis-url] [![Dependency status][dependency-image]][dependency-url]

* Студент: [Yuri_Skasyrski](https://bitbucket.org/Yuri_Skasyrski/.
* Наставник: [Юрий Скасырский](https://htmlacademy.ru/profile/id192386).

---

**Обратите внимание, что папка с вашими исходными файлами — `source/`.**

Полезный файл:

- [Contributing.md](Contributing.md) — руководство по внесению изменений.

_Не удаляйте и не обращайте внимание на файлы:_<br>
_`.editorconfig`, `.gitattributes`, `.gitignore`, `.stylelintrc`, `.travis.yml`, `package-lock.json`, `package.json`._

---

### Памятка

### Проект хранится на git@bitbucket.org:Skasbzhezinski/cat-energy.git

#### 1. Зарегистрируйтесь на Гитхабе

Если у вас ещё нет аккаунта на [github.com](https://github.com/join), скорее зарегистрируйтесь.

#### 2. Создайте форк

[Откройте мастер-репозиторий](https://bitbucket.org/Skasbzhezinski/cat-energy/src/master/) и нажмите кнопку «Fork» в правом верхнем углу. Репозиторий из Академии будет скопирован в ваш аккаунт.

<img width="407" height="154" alt="clone button" src="https://prntscr.com/weasn6">

Получится вот так:

<img width="1143" height="462" alt="clone this repository" src="https://prntscr.com/weavj3">

#### 3. Клонируйте репозиторий на свой компьютер

Будьте внимательны: нужно клонировать свой репозиторий (форк), а не репозиторий Академии. Также обратите внимание, что клонировать репозиторий нужно через SSH, а не через HTTPS. Скопируйте SSH-адрес вашего репозитория:

Клонировать репозиторий можно так:

```
введите в терминале:
git clone SSH-адрес_вашего_форка 
```

Команда клонирует репозиторий на ваш компьютер и подготовит всё необходимое для старта работы.

#### 4. Начинайте обучение!

---

<a href="https://htmlacademy.ru/intensive/adaptive"><img align="left" width="50" height="50" alt="HTML Academy" src="https://up.htmlacademy.ru/static/img/intensive/adaptive/logo-for-github-2.png"></a>

Репозиторий создан для обучения на профессиональном онлайн‑курсе «[HTML и CSS, уровень 2](https://htmlacademy.ru/intensive/adaptive)» от [HTML Academy](https://htmlacademy.ru).

[travis-image]: https://travis-ci.com/htmlacademy-adaptive/192386-pink-16.svg?branch=master
[travis-url]: https://travis-ci.com/htmlacademy-adaptive/192386-pink-16
[dependency-image]: https://david-dm.org/htmlacademy-adaptive/192386-pink-16/dev-status.svg?style=flat-square
[dependency-url]: https://david-dm.org/htmlacademy-adaptive/192386-pink-16?type=dev
